package engine

import (
	"encoding/json"
	"fmt"
	"io"
	"net/url"
	"os"
	"strings"
	"text/tabwriter"

	"github.com/spf13/cobra"

	"gitlab.com/commonground/don/validators/engine/printer"
	"gitlab.com/commonground/don/validators/engine/ruleset"
)

const (
	textFormat = "text"
	jsonFormat = "json"
)

var outputFormats = []string{textFormat, jsonFormat}

func (e *Engine) initCommands() {
	root := &cobra.Command{
		Use:   e.info.Name,
		Short: e.info.Description,
	}
	root.PersistentFlags().String("format", textFormat, fmt.Sprintf("output format (%s)", strings.Join(outputFormats, "|")))

	root.AddCommand(&cobra.Command{
		Use:   "rulesets [ruleset]",
		Short: "List available rulesets",
		Args:  cobra.MaximumNArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			format, _ := cmd.Flags().GetString("format")
			w := cmd.OutOrStdout()

			if len(args) == 1 {
				id := args[0]
				rs := e.rulesetSet.get(id)
				if rs == nil {
					fmt.Fprintf(w, "Ruleset '%s' does not exist\n", id)
					os.Exit(1)
				}
				return printRuleset(rs, cmd.OutOrStdout(), format)
			}

			rss := []*ruleset.Ruleset{}

			for _, id := range e.rulesetSet.ids() {
				rss = append(rss, e.rulesetSet.get(id))
			}

			return printRulesets(rss, cmd.OutOrStdout(), format)
		},
	})

	validate := &cobra.Command{
		Use:           "validate <ruleset> <url>",
		Short:         "Validate an URL against a ruleset",
		SilenceErrors: true,
		Args:          cobra.ExactArgs(2),
		Run:           e.runValidate,
	}

	validateFlags := validate.PersistentFlags()
	validateFlags.SortFlags = false
	validateFlags.Bool("details", false, "Show detailed results")
	validateFlags.Bool("fail-not-passed", false, "Exit with 1 when not passed")

	root.AddCommand(validate)

	root.AddCommand(&cobra.Command{
		Use:   "version",
		Short: "Print version",
		Args:  cobra.NoArgs,
		RunE: func(cmd *cobra.Command, _ []string) error {
			switch format, _ := cmd.Flags().GetString("format"); format {
			case jsonFormat:
				return json.NewEncoder(cmd.OutOrStdout()).Encode(e.info.Build)
			default:
				_, err := fmt.Fprintf(cmd.OutOrStdout(), "Version %s (commit: %s, date: %s)\n", e.info.Build.Version, e.info.Build.Commit, e.info.Build.Date)
				return err
			}
		},
	})

	e.rootCmd = root
}

func (e *Engine) runValidate(cmd *cobra.Command, args []string) {
	rulesetID := args[0]
	rs := e.rulesetSet.get(rulesetID)
	if rs == nil {
		fmt.Printf("Ruleset '%s' does not exist\n", rulesetID)
		os.Exit(2)
	}

	u, err := url.Parse(args[1])
	if err != nil {
		fmt.Printf("Failed to parse URL: %v\n", err)
		os.Exit(2)
	}

	format, _ := cmd.Flags().GetString("format")
	details, _ := cmd.Flags().GetBool("details")
	failNotPassed, _ := cmd.Flags().GetBool("fail-not-passed")

	var p printer.Printer

	switch format {
	case textFormat:
		p = printer.NewText(os.Stdout)
	case jsonFormat:
		p = printer.NewJSON(os.Stdout)
	default:
		fmt.Printf("Unknown output format: %s\n", format)
		os.Exit(2)
	}

	r := ruleset.NewRunner(rs)

	report, err := r.Run(u)
	if err != nil {
		fmt.Printf("Failed to run ruleset: %v\n", err)
		os.Exit(2)
	}

	if err := p.Print(*report, details); err != nil {
		fmt.Printf("Failed to print report: %v\n", err)
		os.Exit(2)
	}

	if failNotPassed && !report.Passed() {
		os.Exit(1)
	}
}

func printRulesets(rss []*ruleset.Ruleset, w io.Writer, format string) error {
	if format == jsonFormat {
		return json.NewEncoder(w).Encode(rss)
	}

	tw := tabwriter.NewWriter(w, 0, 0, 2, ' ', 0)

	f := "%v\t%v\t%v\t%v\t%v\n"
	fmt.Fprintf(tw, f, "Ruleset", "Title", "Version", "Rules", "Documentation URL")
	fmt.Fprintf(tw, f, "-------", "-----", "-------", "-----", "-----------------")

	for _, rs := range rss {
		fmt.Fprintf(tw, f, rs.ID, rs.Title, rs.Version, len(rs.Rules), rs.DocumentationURL)
	}

	return tw.Flush()
}

func printRuleset(rs *ruleset.Ruleset, w io.Writer, format string) error {
	if format == jsonFormat {
		return json.NewEncoder(w).Encode(rs)
	}

	fmt.Fprintf(w, "Ruleset ID: %s\nTitle: %s\nVersion: %s\nDocumentation URL: %s\n\n%s\n\n\n", rs.ID, rs.Title, rs.Version, rs.DocumentationURL, rs.Description)

	tw := tabwriter.NewWriter(w, 0, 0, 2, ' ', 0)

	f := "%v\t%v\t%v\n"
	fmt.Fprintf(tw, f, "Rule ID", "Title", "Documentation URL")
	fmt.Fprintf(tw, f, "-------", "-----", "-----------------")

	for _, r := range rs.Rules {
		fmt.Fprintf(tw, f, r.ID, r.Title, r.DocumentationURL)
	}

	return tw.Flush()
}
