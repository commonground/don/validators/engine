package ruleset

import (
	"net/http"
	"net/url"
	"time"
)

const clientTimeout = time.Second * 5

type Context struct {
	Client  *http.Client
	BaseURL *url.URL
}

func NewContext(c *http.Client, u *url.URL) (*Context, error) {
	return &Context{
		Client:  c,
		BaseURL: u,
	}, nil
}

type Runner struct {
	c  *http.Client
	rs *Ruleset
}

func NewRunner(rs *Ruleset) *Runner {
	return &Runner{
		c: &http.Client{
			Timeout: clientTimeout,
		},
		rs: rs,
	}
}

func (r *Runner) Run(u *url.URL) (*Report, error) {
	testCtx, err := NewContext(r.c, u)
	if err != nil {
		return nil, err
	}

	results, err := r.rs.Run(testCtx)
	if err != nil {
		return nil, err
	}

	return &Report{
		Results: results,
	}, nil
}
