package ruleset_test

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/don/validators/engine/ruleset"
)

type testContext struct{}

func (*testContext) Test(r *ruleset.Rule) ruleset.Result {
	return r.Test.(func() ruleset.Result)()
}

func TestRunner(t *testing.T) {
	rules := []*ruleset.Rule{
		{
			ID: "Rule A",
			Test: func() ruleset.Result {
				return ruleset.Result{Passed: false, Message: "Message A"}
			},
		},
		{
			ID: "Rule B",
			Test: func() ruleset.Result {
				return ruleset.Result{Passed: true, Message: "Message B"}
			},
		},
		{
			ID: "Rule C",
			Test: func() ruleset.Result {
				return ruleset.Result{Passed: false, Message: "Message C"}
			},
		},
	}

	rs := &ruleset.Ruleset{Rules: rules, RuleTester: func(ctx *ruleset.Context) (ruleset.RuleTester, error) { return &testContext{}, nil }}
	r := ruleset.NewRunner(rs)
	report, err := r.Run(&url.URL{})

	assert.NoError(t, err)
	assert.Len(t, report.Results, 3)

	expected := []ruleset.Result{
		{Passed: false, Message: "Message A", Rule: rules[0]},
		{Passed: true, Message: "Message B", Rule: rules[1]},
		{Passed: false, Message: "Message C", Rule: rules[2]},
	}

	assert.Equal(t, expected, report.Results)
}
