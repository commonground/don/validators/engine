package ruleset_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/don/validators/engine/ruleset"
)

func TestReportPassed(t *testing.T) {
	assert.True(t, ruleset.Report{}.Passed())

	r := ruleset.Report{
		Results: []ruleset.Result{
			{Passed: true},
			{Passed: true},
		},
	}
	assert.True(t, r.Passed())

	r = ruleset.Report{
		Results: []ruleset.Result{
			{Passed: false},
			{Passed: true},
		},
	}
	assert.False(t, r.Passed())
}
