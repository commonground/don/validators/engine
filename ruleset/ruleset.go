package ruleset

type Ruleset struct {
	ID               string  `json:"ruleset_id"`
	Title            string  `json:"title"`
	Description      string  `json:"description"`
	Version          string  `json:"version"`
	DocumentationURL string  `json:"documentation_url"`
	Rules            []*Rule `json:"rules"`

	RuleTester func(*Context) (RuleTester, error) `json:"-"`
}

func (rs *Ruleset) Run(ctx *Context) ([]Result, error) {
	tester, err := rs.RuleTester(ctx)
	if err != nil {
		return nil, err
	}

	results := make([]Result, len(rs.Rules))
	for i, rule := range rs.Rules {
		result := tester.Test(rule)
		result.Rule = rule
		results[i] = result
	}

	return results, nil
}

type RuleTester interface {
	Test(*Rule) Result
}

func NewRule(id, title, description, url string, test any) *Rule {
	return &Rule{ID: id, Title: title, Description: description, DocumentationURL: url, Test: test}
}

type Rule struct {
	ID               string `json:"rule_id"`
	Title            string `json:"title"`
	Description      string `json:"description"`
	DocumentationURL string `json:"documentation_url"`

	Test any `json:"-"`
}

type Result struct {
	Passed  bool   `json:"passed"`
	Message string `json:"message"`
	Details string `json:"details"`
	Rule    *Rule  `json:"-"`
}

type Report struct {
	Results []Result
}

func (r Report) Passed() bool {
	for _, result := range r.Results {
		if !result.Passed {
			return false
		}
	}

	return true
}
