# Validator engine

Library for creating Validators.


## Licence

Copyright © Stichting Geonovum 2023

[Licensed under the EUPLv1.2](LICENCE.md)
