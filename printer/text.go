package printer

import (
	"fmt"
	"io"
	"text/tabwriter"

	"gitlab.com/commonground/don/validators/engine/ruleset"
)

type TextPrinter struct {
	w io.Writer
}

func NewText(w io.Writer) *TextPrinter {
	return &TextPrinter{w: w}
}

func (p *TextPrinter) Print(report ruleset.Report, details bool) error {
	w := tabwriter.NewWriter(p.w, 0, 0, 2, ' ', 0)
	f := "%v\t%v\t%v\n"
	fmt.Fprintf(w, f, "Rule ID", "Passed", "Message")
	fmt.Fprintf(w, f, "-------", "------", "-------")

	for _, result := range report.Results {
		fmt.Fprintf(w, f, result.Rule.ID, YesNo(result.Passed), result.Message)
	}

	if err := w.Flush(); err != nil {
		return err
	}

	fmt.Fprintln(p.w)

	if report.Passed() {
		fmt.Fprintln(p.w, "Validation passed")
	} else {
		fmt.Fprintln(p.w, "Validation failed")
	}

	if details {
		for _, result := range report.Results {
			if result.Passed && result.Details == "" {
				continue
			}

			fmt.Fprintf(p.w, "\nRule ID: %s\nDetails:\n%s\n", result.Rule.ID, result.Details)
		}
	}

	return nil
}

func YesNo(v bool) string {
	if v {
		return "Yes"
	}
	return "No"
}
