package printer_test

import (
	"gitlab.com/commonground/don/validators/engine/ruleset"
)

var testReport = ruleset.Report{
	Results: []ruleset.Result{
		{Rule: &ruleset.Rule{ID: "Test-01"}, Passed: true, Message: ""},
		{Rule: &ruleset.Rule{ID: "Test-02"}, Passed: false, Message: "Failure message", Details: "Some details"},
		{Rule: &ruleset.Rule{ID: "Test-03"}, Passed: true, Message: "Another message"},
		{Rule: &ruleset.Rule{ID: "Test-04"}, Passed: false, Message: "", Details: "Other details"},
	},
}
