package printer

import (
	"encoding/json"
	"io"

	"gitlab.com/commonground/don/validators/engine/ruleset"
)

type Result struct {
	RuleID string `json:"rule_id"`
	ruleset.Result
}

type JSONPrinter struct {
	w io.Writer
}

func NewJSON(w io.Writer) *JSONPrinter {
	return &JSONPrinter{w: w}
}

func (p *JSONPrinter) Print(report ruleset.Report, _ bool) error {
	results := make([]Result, len(report.Results))

	for i, r := range report.Results {
		results[i] = Result{
			RuleID: r.Rule.ID,
			Result: r,
		}
	}

	return json.NewEncoder(p.w).Encode(results)
}
