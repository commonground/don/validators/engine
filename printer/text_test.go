package printer_test

import (
	"bytes"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/don/validators/engine/printer"
	"gitlab.com/commonground/don/validators/engine/ruleset"
)

func TestText(t *testing.T) {
	buf := &bytes.Buffer{}
	p := printer.NewText(buf)

	t.Run("Passed", func(t *testing.T) {
		successReport := ruleset.Report{
			Results: []ruleset.Result{
				{Rule: &ruleset.Rule{ID: "Test-01"}, Passed: true, Message: ""},
				{Rule: &ruleset.Rule{ID: "Test-02"}, Passed: true, Message: "Another message"},
			},
		}

		buf.Reset()
		err := p.Print(successReport, false)
		require.NoError(t, err)

		expected := []string{
			"Rule ID  Passed  Message",
			"-------  ------  -------",
			"Test-01  Yes     ",
			"Test-02  Yes     Another message",
			"",
			"Validation passed",
			"",
		}
		assert.Equal(t, strings.Join(expected, "\n"), buf.String())
	})

	t.Run("Failed", func(t *testing.T) {
		buf.Reset()
		err := p.Print(testReport, false)
		require.NoError(t, err)

		expected := []string{
			"Rule ID  Passed  Message",
			"-------  ------  -------",
			"Test-01  Yes     ",
			"Test-02  No      Failure message",
			"Test-03  Yes     Another message",
			"Test-04  No      ",
			"",
			"Validation failed",
			"",
		}
		assert.Equal(t, strings.Join(expected, "\n"), buf.String())
	})

	t.Run("Failed With details", func(t *testing.T) {
		buf.Reset()
		err := p.Print(testReport, true)
		require.NoError(t, err)

		expected := []string{
			"Rule ID  Passed  Message",
			"-------  ------  -------",
			"Test-01  Yes     ",
			"Test-02  No      Failure message",
			"Test-03  Yes     Another message",
			"Test-04  No      ",
			"",
			"Validation failed",
			"",
			"Rule ID: Test-02",
			"Details:",
			"Some details",
			"",
			"Rule ID: Test-04",
			"Details:",
			"Other details",
			"",
		}
		assert.Equal(t, strings.Join(expected, "\n"), buf.String())
	})
}
