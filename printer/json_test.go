package printer_test

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/don/validators/engine/printer"
)

func TestJSON(t *testing.T) {
	buf := &bytes.Buffer{}
	p := printer.NewJSON(buf)

	err := p.Print(testReport, true)
	require.NoError(t, err)

	expected := `[{"rule_id":"Test-01","passed":true,"message":"","details":""},{"rule_id":"Test-02","passed":false,"message":"Failure message","details":"Some details"},{"rule_id":"Test-03","passed":true,"message":"Another message","details":""},{"rule_id":"Test-04","passed":false,"message":"","details":"Other details"}]` + "\n"
	assert.Equal(t, expected, buf.String())
}
