package printer

import "gitlab.com/commonground/don/validators/engine/ruleset"

type Printer interface {
	Print(report ruleset.Report, details bool) error
}
