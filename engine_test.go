package engine

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/don/validators/engine/ruleset"
)

func TestEngine_RegisterRuleset(t *testing.T) {
	e := New("", "", BuildInfo{})
	rs := &ruleset.Ruleset{ID: "test"}

	e.RegisterRuleset(rs)

	assert.Panics(t, func() { e.RegisterRuleset(rs) })
}

func TestRulesetSet(t *testing.T) {
	rss := newRulesetSet()

	rs := &ruleset.Ruleset{ID: "test-a"}
	rss.add(rs)
	rss.add(&ruleset.Ruleset{ID: "test-b"})
	rss.add(&ruleset.Ruleset{ID: "an-other"})

	t.Run("Exists", func(t *testing.T) {
		assert.NotNil(t, rss.get("test-a"))
	})

	t.Run("Does not exits", func(t *testing.T) {
		assert.Nil(t, rss.get("other"))
	})

	t.Run("Duplicate", func(t *testing.T) {
		assert.Panics(t, func() { rss.add(rs) })
		assert.Panics(t, func() { rss.add(&ruleset.Ruleset{ID: "test-a"}) })
	})

	t.Run("IDs order", func(t *testing.T) {
		assert.Equal(t, []string{"an-other", "test-a", "test-b"}, rss.ids())
	})
}
