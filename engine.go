package engine

import (
	"fmt"
	"os"
	"sort"
	"sync"

	"github.com/spf13/cobra"
	"gitlab.com/commonground/don/validators/engine/ruleset"
)

type BuildInfo struct {
	Version string `json:"version"`
	Commit  string `json:"commit"`
	Date    string `json:"date"`
}

type info struct {
	Name        string
	Description string
	Build       BuildInfo
}

type Engine struct {
	info       info
	rootCmd    *cobra.Command
	rulesetSet rulesetSet
}

func (e *Engine) RegisterRuleset(r *ruleset.Ruleset) {
	e.rulesetSet.add(r)
}

func (e *Engine) Run() {
	if err := e.rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

func New(name, description string, build BuildInfo) *Engine {
	e := &Engine{
		info: info{
			Name:        name,
			Description: description,
			Build:       build,
		},
		rulesetSet: newRulesetSet(),
	}

	e.initCommands()

	return e
}

type rulesetSet struct {
	mu sync.RWMutex
	m  map[string]*ruleset.Ruleset
}

func newRulesetSet() rulesetSet {
	return rulesetSet{
		m: make(map[string]*ruleset.Ruleset),
	}
}

func (r *rulesetSet) add(rs *ruleset.Ruleset) {
	r.mu.Lock()
	defer r.mu.Unlock()

	if _, ok := r.m[rs.ID]; ok {
		panic(fmt.Sprintf("Ruleset '%s' already exists", rs.ID))
	}

	r.m[rs.ID] = rs
}

func (r *rulesetSet) get(id string) *ruleset.Ruleset {
	r.mu.RLock()
	rs := r.m[id]
	r.mu.RUnlock()
	return rs
}

func (r *rulesetSet) ids() []string {
	ids := make([]string, 0, len(r.m))
	r.mu.RLock()
	for id := range r.m {
		ids = append(ids, id)
	}
	r.mu.RUnlock()
	sort.Strings(ids)
	return ids
}
